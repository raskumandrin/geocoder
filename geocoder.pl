#!/usr/bin/perl

use strict;
use LWP::UserAgent;

die `pod2text $0` if (scalar @ARGV) == 0;

my $ua = LWP::UserAgent->new;
$ua->agent('Geocoder robot: moscow.address@yandex.ru');

print "Точность\tИсходный адрес\tПолученный адрес\tДолгота\tШирота\n";

while (<>) {
	chomp;
	my $address = $_;
	$address =~ s/[\s"']+/ /g;
	my ($longitude,$latitude,$fixed_address,$precision);
	my $response = $ua->get("http://geocode-maps.yandex.ru/1.x/?geocode=$address\&key=ABy3b04BAAAABuv_fAIA-k1-nvBwWFcK6-alxOUpyz40OWcAAAAAAAAAAACyp6uclOYi7HqtwMSHnH1SGJTpmQ==\&results=1");
	($longitude,$latitude) = ($response->content =~ m!<pos>(.*) (.*)</pos>!);
	if ( $longitude and $latitude ) {
		$response = $ua->get("http://geocode-maps.yandex.ru/1.x/?geocode=$longitude,$latitude\&key=ABy3b04BAAAABuv_fAIA-k1-nvBwWFcK6-alxOUpyz40OWcAAAAAAAAAAACyp6uclOYi7HqtwMSHnH1SGJTpmQ==\&results=1");
		($fixed_address,$precision) = ($response->content =~ m!<text>(.*)</text>.*<precision>(.*)</precision>!s);
	};
	
	my $precision_value = 
		$precision eq 'exact'  ? 'Точное соответствие' : 
		$precision eq 'number' ? 'Совпадает только номер дома' : 
		$precision eq 'near'   ? 'Найден дом поблизости' : 
		$precision eq 'street' ? 'Найдена улица' : 
		$precision eq 'other'  ? 'Улица не найдена'
							   : 'Не найдено соответствия';
		
	print "$precision_value\t$address\t$fixed_address\t$longitude\t$latitude\n";
}

__END__

=head1 NAME
  Геокодер

=head1 SYNOPSYS
  geocoder.pl
    запуск без параметров: выведется справка
  geocoder.pl address.txt > geo-address.txt
    запуск с параметром (файлом с адресами - один адрес в каждой строке):
    результат работы выводится в стандартный выходной поток

=head1 DESCRIPTION
  Преобразование адреса в гео-координаты.
  Обратное преобразование координат в адрес (для нормализации адреса).
  http://api.yandex.ru/maps/geocoder/doc/desc/concepts/About.xml

=head1 AUTHOR
  Stas Raskumandrin <stas@raskumandrin.ru>, http://stas.raskumandrin.ru

=cut