#!/usr/bin/perl

use strict;
use LWP::UserAgent;

die `pod2text $0` if (scalar @ARGV) == 0;

my $ua = LWP::UserAgent->new;
$ua->agent('Geocoder robot: moscow.address@yandex.ru');

print "Точность\tИсходный адрес\tПолученный адрес\tДолгота\tШирота\n";

while (<>) {
#	chomp;
	my $address = $_;
	$address =~ s/^\xef\xbb\xbf//g;
	$address =~ s/^\s+//g;
	$address =~ s/[\x0A\x0D]//g; 
	$address =~ s/\s+$//g;
	my $address1 = $address;
	$address =~ s/[\s"']+/\+/g;
	my ($longitude,$latitude,$fixed_address,$precision);
#	my $url = "http://maps.googleapis.com/maps/api/geocode/json?language=ru\&address=$address\&sensor=false";
#	my $url = "http://maps.googleapis.com/maps/api/geocode/json?language=ru\&address=Украина,+Харьков,+ул.+Сумская,+60\&sensor=false";
#	print "$url\n";
#	my $response = $ua->get($url);
	my $response = $ua->get("http://maps.googleapis.com/maps/api/geocode/json?language=ru\&address=$address\&sensor=false");
sleep(1);
#	print $response->content; exit;
	($fixed_address,$longitude,$latitude,$precision) = ($response->content =~ m!formatted_address"\s*:\s*"([^"]+)".*location"[^"]+"lat"\s+:\s+([^,]+),\s*"lng"\s+:\s+(\S+)\s+},\s+"location_type"\s:\s"([^"]+)"!s);
#	if ( $longitude and $latitude ) {
#		$response = $ua->get("http://geocode-maps.yandex.ru/1.x/?geocode=$longitude,$latitude\&key=ABy3b04BAAAABuv_fAIA-k1-nvBwWFcK6-alxOUpyz40OWcAAAAAAAAAAACyp6uclOYi7HqtwMSHnH1SGJTpmQ==\&results=1");
#		($fixed_address,$precision) = ($response->content =~ m!<text>(.*)</text>.*<precision>(.*)</precision>!s);
#	};
#print "$fixed_address\n$longitude\n$latitude\n$precision\n\n"; exit;
	my $precision_value = 
		$precision eq 'ROOFTOP'  ? 'Точное соответствие' : 
		$precision eq 'RANGE_INTERPOLATED' ? 'Найден дом поблизости (интерполяция)' :
		$precision eq 'GEOMETRIC_CENTER' ? 'Найдена улица' : 
		$precision eq 'APPROXIMATE'  ? 'Приблизительно'
							   : 'Не найдено соответствия';
#							   print "$address1\n";
	print "$precision_value\t$address1\t$fixed_address\t$longitude\t$latitude\n";
}

__END__

=head1 NAME
  Геокодер

=head1 SYNOPSYS
  geocoder.pl
    запуск без параметров: выведется справка
  geocoder.pl address.txt > geo-address.txt
    запуск с параметром (файлом с адресами - один адрес в каждой строке):
    результат работы выводится в стандартный выходной поток

=head1 DESCRIPTION
  Преобразование адреса в гео-координаты.
  Обратное преобразование координат в адрес (для нормализации адреса).
  http://api.yandex.ru/maps/geocoder/doc/desc/concepts/About.xml

=head1 AUTHOR
  Stas Raskumandrin <stas@raskumandrin.ru>, http://stas.raskumandrin.ru

=cut