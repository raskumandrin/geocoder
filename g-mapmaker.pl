#!/usr/bin/perl

use strict;

die `pod2text $0` if (scalar @ARGV) == 0;

my $point_list_string = '';
my $init_collection_list_string = '';
my $show_collection_list_string = '';
my $check_function_list_string = '';
my $check_input_list_string = '';


my %geo;

my ($style_id,$current_style_id,$placemark_id,$scolor) = (0,0,0,''); 

while (<>) {

	my ($address,$longitude,$latitude,$point_style,$point_name) = ( m/^(.+)\s(\d+\.\d+)\s(\d+\.\d+)(?:\s(\w+)\s+(.+))?[\s\t\r\n]*?$/ );

	if ( $point_style and $point_name ) {
		$current_style_id = ++$style_id;
#		var markers = [];
		
		$init_collection_list_string .= 
			"var markers$style_id = [];\n";
#	    $show_collection_list_string .=
#			"myMap.geoObjects.add(myCollection_$style_id)\n";
			
		$check_function_list_string .= << "EOF";
		
		\$('#chb_$style_id').change(function(){

			if (\$("input#chb_$style_id:checked").length) {
			    for (var k = 0; k < markers$style_id.length; k++) {
			           markers${style_id}[k].setVisible(true);

			       }  
			}
			else {
			    for (var k = 0; k < markers$style_id.length; k++) {
			           markers${style_id}[k].setVisible(false);

			       }  
			} 
		});
		
EOF

		my $image = $point_style;
		if ( $image =~ /^(.*)Dot$/ ) {
			$image = "dot$1";
		}
		$scolor = $image;
		
		$check_input_list_string .= <<"EOF";

		<label style="padding-right: 2em;">
		<input type="checkbox" id="chb_$style_id" checked>
		<img style="height: 20px; margin-bottom: -6px;" src="http://maps.google.com/intl/en_us/mapfiles/ms/micons/$image.png">
		$point_name
		</label>

EOF

	}
	
	if ( $address and $longitude and $latitude ) {
	
		($geo{min_lon} = $geo{max_lon} = $longitude ) if not( $geo{min_lon} or $geo{max_lon} );
		($geo{min_lat} = $geo{max_lat} = $latitude) if not( $geo{min_lat} or $geo{max_lat} );
	
		$geo{min_lon} = $longitude if $longitude < $geo{min_lon};
		$geo{min_lat} = $latitude if $latitude < $geo{min_lat};

		$geo{max_lon} = $longitude if $longitude > $geo{max_lon};
		$geo{max_lat} = $latitude if $latitude > $geo{max_lat};
	
		$placemark_id++;

		$point_list_string .= <<"EOF";
		
		
		var marker$placemark_id = new google.maps.Marker({
	         position: new google.maps.LatLng(
				$longitude,$latitude
			),
	         map: map,
			 icon: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/$scolor.png',
			 
	         title:"$address"
	     });
		 var infowindow$placemark_id = new google.maps.InfoWindow({
		  content: "$address"
		 });

		 google.maps.event.addListener(marker$placemark_id, 'click', function() {
		   infowindow.close();
		   infowindow.content = marker$placemark_id.title;
		   infowindow.open(map, marker$placemark_id);
		 });
		 
		 markers$current_style_id.push(marker$placemark_id);
		
		marker$placemark_id.setVisible(true);
EOF
	}	

	
}

$geo{cen_lon} = $geo{min_lon} + ( $geo{max_lon} - $geo{min_lon} ) / 2;
$geo{cen_lat} = $geo{min_lat} + ( $geo{max_lat} - $geo{min_lat} ) / 2;


while (<DATA>) {
  	
	last if /__END__/;

  	if (/_POINTS_/) {
	  	print $point_list_string;
  	}
  	elsif (/_INIT_COLLECTIONS_/) {
	  	print $init_collection_list_string;
  	}
  	elsif (/_SHOW_COLLECTIONS_/) {
	  	print $show_collection_list_string;
  	}
  	elsif (/_CHECK_FUNCTIONS_/) {
		print $check_function_list_string;
  	}
    elsif (/_CHECK_INPUTS_/) {
	 	print $check_input_list_string;
  	}
  	elsif (/_CENTER_/) {
		print  " $geo{cen_lon},$geo{cen_lat} \n";
  	}
  	else {
	  	print "$_";
  	}
}

# makermap470gmail.com
# golosneba
# api key: AIzaSyASsTLKP3r8H2EgIn09GeZTh0aGRiqvEN8


__DATA__
<!DOCTYPE html>
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8"><html>
  
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map_canvas { height: 100% }
	  html,body {margin: 0};
    </style>
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyASsTLKP3r8H2EgIn09GeZTh0aGRiqvEN8&sensor=true">
    </script>
	<script charset="utf-8" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
	
    <script type="text/javascript">
_INIT_COLLECTIONS_
      function initialize() {
        var mapOptions = {
          center: new google.maps.LatLng(
_CENTER_
		  ),
          zoom: 12,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);
		 var infowindow = new google.maps.InfoWindow({
		  content: "Hello <b>World</b>!"
		 });
			
_POINTS_
		
		
		
      }

	  $(document).ready(function() {
	  
_CHECK_FUNCTIONS_
});

    </script>
  </head>
  <body onload="initialize()">

	<div  id="map_canvas" style="position: absolute; top: 60px; bottom: 0px; left:0; right: 0; border-top: solid 1px silver;"></div>
	
	<div style="position: absolute; top: 0; height: 60px; left:0; right: 0; padding: 10px;">
	_CHECK_INPUTS_
	</div>
	
  </body>
</html>

__END__

=head1 NAME
  Генератор карты

=head1 SYNOPSYS
  mapmaker.pl
    запуск без параметров: выведется справка
  mapmaker.pl address.txt > map.html
    запуск с параметром:
    результат работы (файл с картой) выводится в стандартный выходной поток

=head1 DESCRIPTION
  Построение карты с метками по исходному файлу с гео-координатами.

  Формат файла:
    значения, разделённые табуляциями:
    адрес; широта; долгота

	эти строки разделены в группы.
	
	каждая группа инициализируется строкой, разделённой табулияцией
	цвет метки (точнее, тип); тип объекта
	
	пример:
	
	Липецк, ДЕПУТАТСКАЯ, 94	39.563692	52.593265	purple	Ларьки
	Липецк, ПОБЕДЫ, 91А	39.546435	52.584577		
	Липецк, ПОБЕДЫ, 27	39.570034	52.596602		
	Липецк, ПОБЕДЫ, 104	39.555319	52.590097		
	Липецк, ПОБЕДЫ, 72	39.563305	52.594873	blue	Оптовые магазины
	Липецк, ЦИОЛКОВСКОГО, 13	39.557799	52.611620		
	Липецк, ПЕТРА СМОРОДИНА, 24	39.533742	52.588844
	
  Цвета меток:
  
    http://maps.google.com/intl/en_us/mapfiles/ms/micons/purple.png
    http://maps.google.com/intl/en_us/mapfiles/ms/micons/yellow.png
    http://maps.google.com/intl/en_us/mapfiles/ms/micons/blue.png
    http://maps.google.com/intl/en_us/mapfiles/ms/micons/green.png
    http://maps.google.com/intl/en_us/mapfiles/ms/micons/red.png
    http://maps.google.com/intl/en_us/mapfiles/ms/micons/orange.png
  
    для метки в исходном файле должно быть указано значение цвета - «purple»

=head1 AUTHOR
  Stas Raskumandrin <stas@raskumandrin.ru>, http://stas.raskumandrin.ru

=cut